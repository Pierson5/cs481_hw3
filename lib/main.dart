import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new ScarlettApp(),
    );
  }
}

class ScarlettApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<ScarlettApp> {
  //array('list' in dart) of photo descriptions/locations
  List<String> photoDescriptions = [
    'October 2018, La Jolla Acquarium. Age 2',
    '2019, Cruisin Grand. Age 3',
    'March 2019, Walmart. Age 3 (Turning 4)',
    'January 2019, Escondido Sprinter. Age 3',
    'April (Easter) 2019, Grandmas House. Age 3',
    'January 2020, Escondido Childrens Museum. Age 3',
    'January 2020, Pre-School Photos. Age 3',
    'January, 2020, Grape Day Park. Age 3',
    'March 2020, Oregon. Age 4',
  ];
  List<String> photos = [
    'images/1.jpg',
    'images/2.jpg',
    'images/2.5.jpg',
    'images/3.jpg',
    'images/4.jpg',
    'images/5.jpg',
    'images/7.jpg',
    'images/8.jpg',
    'images/6.jpg',
  ];
  int photoIndex = 0; //first image

  //function to switch to previous image
  void _previousPhoto() {
    setState(() {
      //check if photo is the first photo, can't go to previous if first image
      //If second image, take back to first image. etc.
      photoIndex = photoIndex > 0 ? photoIndex - 1 : 0;
    });
  }

  //function to switch to next image
  void _nextPhoto() {
    setState(() {
      //photos.length -1 is the last image (starts at 0).
      //if photoIndex is < the last image, increment. Otherwise stay at photoIndex
      photoIndex = photoIndex < photos.length - 1 ? photoIndex + 1 : photoIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Scarlett Pierson'),
        ),
        body: Column(
          children: <Widget>[
            //wrap image into a container, give container dimensions
            //overflowBox allows the width of the image to overflow the boundaries
            //of the container, but will not overflow height.

            //Container for images
            Container(
                height: 600,
                child: new OverflowBox(
                    minWidth: 0.0,
                    minHeight: 0.0,
                    maxWidth: double.infinity,
                    child: new Image(
                        image: new AssetImage(photos[photoIndex]),
                        fit: BoxFit.cover))),

            //Container for description
            Container(
              padding: const EdgeInsets.all(10),
              child: Text(
                photoDescriptions[photoIndex],
                softWrap: true,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text('Prev'),
                  onPressed: _previousPhoto,
                  elevation: 5.0,
                  color: Colors.pinkAccent,
                ),
                //add space between buttons with SizedBox
                SizedBox(width: 75.0),
                RaisedButton(
                  child: Text('Next'),
                  onPressed: _nextPhoto,
                  elevation: 5.0,
                  color: Colors.pinkAccent,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
} //MyApp
